﻿using ANW.ComposerApp.Services.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ANW.ComposerApp.Services
{
    public class ComposerService : IComposerService
    {
        private readonly IRepository<Composer> _composerRepository;

        public ComposerService(IRepository<Composer> composerRepository)
        {
            _composerRepository = composerRepository;
        }

        public Task<Composer> Get(int id)
        {
            return _composerRepository.Get(id);
        }

        public Task<IEnumerable<Composer>> GetAll()
        {
            // todo: handle async properly (e.g. with EF, use ToListAsync)
            return Task.FromResult(_composerRepository.Query().ToList().AsEnumerable());
        }
    }
}
