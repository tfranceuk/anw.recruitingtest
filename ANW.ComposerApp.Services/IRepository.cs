﻿using System.Linq;
using System.Threading.Tasks;

namespace ANW.ComposerApp.Services
{
    public interface IRepository<T>
    {
        Task<T> Get(int id);
        IQueryable<T> Query();

        void Store(T entity);

        // todo: save could be handled by unit of work somehow
        Task SaveChanges();
    }
}
