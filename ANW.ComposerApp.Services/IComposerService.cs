﻿using ANW.ComposerApp.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ANW.ComposerApp.Services
{
    public interface IComposerService
    {
        Task<IEnumerable<Composer>> GetAll();
        Task<Composer> Get(int id);
    }
}
