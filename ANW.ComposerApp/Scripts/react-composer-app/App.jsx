﻿import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router } from "react-router-dom";
import Routes from './Routes.jsx';

const App = () => (
    <Router>
        <Routes />
    </Router>
);


var f2 = fetch('/api/composers/1').then(x => x.json()).then(x => console.log(x));

ReactDOM.render(<App/>, document.getElementById('app'));