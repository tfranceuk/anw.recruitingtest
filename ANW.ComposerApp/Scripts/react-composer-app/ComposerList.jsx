﻿import React from 'react';
import { Link } from "react-router-dom";

const Layout = ({ children }) => (
    <div>
        <h3>Composers</h3>
        {children}
    </div>
);

export default class extends React.Component {
    constructor(props) {
        super(props);

        this.state = { loading: true };
    }

    componentDidMount() {
        this.loadData();
    }

    render() {
        return (<Layout>{this.renderContent()}</Layout>)
    }

    renderContent() {
        if (this.state.loading)
            return (<span>Loading...</span>);

        return this.renderList(this.state.data);
    }

    renderList(data) {
        return (
            <div>
                <ul className="list-unstyled">
                    {data.map(x => (<li key={x.Id}><Link to={`/${x.Id}`}>{`${x.FirstName} ${x.LastName}`}</Link></li>))}
                </ul>
                <p>{data.length} results</p>
            </div>
        );
    }

    loadData() {
        // todo: move api calls into service
        fetch('/api/composers')
            .then(x => x.json())
            .then(x => this.setState({ loading: false, data: x }));
    }
}