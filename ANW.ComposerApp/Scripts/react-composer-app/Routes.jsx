﻿import React from 'react';
import { Route } from "react-router-dom";
import ComposerList from './ComposerList.jsx';
import ComposerView from './ComposerView.jsx';

export default () => (
    <div>
        <Route exact path="/" component={ComposerList} />
        <Route path="/:id" component={ComposerView} />
    </div>
);