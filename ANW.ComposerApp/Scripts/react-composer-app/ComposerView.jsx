﻿import React from 'react';
import { Link } from "react-router-dom";

const Layout = ({ children }) => (
    <div>
        <h3>Composer</h3>
        {children}
    </div>
);

export default class extends React.Component {
    constructor(props) {
        super(props);

        this.state = { loading: true, id: props.match.params.id };
    }

    componentDidMount() {
        this.loadData(this.state.id);
    }

    render() {
        return (<Layout>{this.renderContent()}</Layout>)
    }

    renderContent() {
        if (this.state.loading)
            return (<span>Loading...</span>);

        const d = this.state.data;
        return (
            <div>
                <div className="form-group row">
                    <label className="col-md-2 col-form-label">Title</label>
                    <div className="col-md-10">
                        <p className="form-control-static">{d.Title}</p>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-md-2 col-form-label">Name</label>
                    <div className="col-md-10">
                        <p className="form-control-static">{`${d.FirstName} ${d.LastName}`}</p>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-md-2 col-form-label">Awards</label>
                    <div className="col-md-10">
                        <p className="form-control-static">{d.Awards}</p>
                    </div>
                </div>
            </div>
        );
    }

    loadData(id) {
        // todo: move api calls into service
        fetch('/api/composers/'+id)
            .then(x => x.json())
            .then(x => this.setState({ loading: false, data: x }));
    }
}