﻿using ANW.ComposerApp.Data;
using ANW.ComposerApp.Services;
using System.Threading.Tasks;
using System.Web.Http;

namespace ANW.ComposerApp.Api
{
    public class ComposersController : ApiController
    {
        private readonly IComposerService _composerService;

        // todo: use proper dependency inversion and injection via DependencyResolver and Ninject
        //       I had issues getting this integrated, so decided to leave out for now
        //       (see alternate commented constructor dependency inverted form)
        public ComposersController()
        {
            _composerService = new ComposerService(new ComposerRepository());
        }

        //public ComposersController(IComposerService composerService)
        //{
        //    _composerService = composerService;
        //}

        // todo: map to viewmodels of some sort

        public async Task<IHttpActionResult> GetAll()
        {
            var items = await _composerService.GetAll();
            return Ok(items);
        }

        public async Task<IHttpActionResult> Get(int id)
        {
            var item = await _composerService.Get(id);
            return Ok(item);
        }
    }
}
