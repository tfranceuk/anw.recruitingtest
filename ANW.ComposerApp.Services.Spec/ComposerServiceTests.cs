﻿using ANW.ComposerApp.Services.Models;
using Moq;
using Should;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ANW.ComposerApp.Services.Spec
{
    public class ComposerServiceTests
    {
        private List<Composer> _items;
        private ComposerService _sut;
        private IEnumerable<Composer> _result;

        private void Given()
        {
            _items = new List<Composer>
            {
                new Composer {Id = 1, FirstName = "First", LastName = "Last", Title = "Mr", Awards = "Awesome Award"}
            };
            var moqRepo = new Mock<IRepository<Composer>>();
            moqRepo.Setup(x => x.Query()).Returns(_items.AsQueryable());

            _sut = new ComposerService(moqRepo.Object);
        }

        private async Task When()
        {
            _result = await _sut.GetAll();
        }

        [Fact]
        public async Task result_should_not_be_null()
        {
            Given();
            await When();

            _result.ShouldNotBeNull();
        }

        [Fact]
        public async Task result_should_contain_expected_number_of_items()
        {
            Given();
            await When();

            _result.Count().ShouldEqual(_items.Count);
        }
    }
}
