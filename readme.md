# ANW Recruitment test

## What I've done
* restructured sln into multiple projects
  * Services, containing core functionatlity
  * Services.Spec, containing tests for Services
  * Data, containing repository implementation for static composer data
* added web api 2 to web proj, to serve data to react app
* implemented changes in react app to consume api, and present data
* use of react router for navigation to single composer view, from composer list

## What I'd do next
* implement dependency injection (using likely ninject) in web app.
  * (I was having problems getting this working, so decided to skip for now to save time.  see comments in ComposersController)
* api service/handler in react, so centralise api functionality
* use of react-redux to help with state management
* separation of view components and container components
* **Please see `// todo`s in code for extra notes**
